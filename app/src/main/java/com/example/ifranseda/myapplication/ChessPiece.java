package com.example.ifranseda.myapplication;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.util.TypedValue;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by ifranseda on 3/27/16.
 */
public class ChessPiece {

    public static ImageView create(Context context, String config) {
        Log.d("PIECE", config);

        char pieceType = config.charAt(0);
        char horizontalPosition = config.charAt(1);
        char verticalPosition = config.charAt(2);

        int pieceImageResource = 0;
        if ('K' == pieceType) {
            pieceImageResource = R.drawable.white_king;
        } else if ('Q' == pieceType) {
            pieceImageResource = R.drawable.white_queen;
        } else if ('B' == pieceType) {
            pieceImageResource = R.drawable.white_bishop;
        } else if ('N' == pieceType) {
            pieceImageResource = R.drawable.white_knight;
        } else if ('R' == pieceType) {
            pieceImageResource = R.drawable.white_rook;
        } else if ('k' == pieceType) {
            pieceImageResource = R.drawable.black_king;
        } else if ('q' == pieceType) {
            pieceImageResource = R.drawable.black_queen;
        } else if ('b' == pieceType) {
            pieceImageResource = R.drawable.black_bishop;
        } else if ('n' == pieceType) {
            pieceImageResource = R.drawable.black_knight;
        } else if ('r' == pieceType) {
            pieceImageResource = R.drawable.black_rook;
        }

        int horizontal = horizontalPosition - Integer.valueOf('a');
        int vertical = Math.abs(verticalPosition - Integer.valueOf('1') - 8) - 1;

        ImageView piece = new ImageView(context);
        piece.setImageResource(pieceImageResource);
        piece.setAdjustViewBounds(true);
        piece.setScaleType(ImageView.ScaleType.FIT_CENTER);

        int size = (int) context.getResources().getDimension(R.dimen.grid_size);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(
                size,
                size);

        layoutParams.leftMargin = getPixels(context, 28) + (horizontal * size);
        layoutParams.topMargin = getPixels(context, 28) + (vertical * size);

        piece.setLayoutParams(layoutParams);

        return piece;
    }

    private static int getPixels(Context context, int dipValue) {
        Resources r = context.getResources();
        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dipValue, r.getDisplayMetrics());
        return px;
    }
}
