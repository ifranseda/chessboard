package com.example.ifranseda.myapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by ifranseda on 3/27/16.
 */
public class MainActivity extends AppCompatActivity implements Callback {

    private Button mStart;
    private FrameLayout mContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStart = (Button) findViewById(R.id.start);
        mContainer = (FrameLayout) findViewById(R.id.container);

        mStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (App.getInstance().isConnected()) {
                    App.getInstance().stopSocket();
                    mStart.setText("START");
                } else {
                    App.getInstance().prepareSocket();
                    mStart.setText("STOP");
                }
            }
        });

        App.getInstance().addListener(this);

        if (App.getInstance().isConnected()) {
            mStart.setText("STOP");
        } else {
            mStart.setText("START");
        }
    }

    @Override
    public void onReceive(String message) {
        draw(message);
    }

    void draw(String message) {
        Log.d("XXX", message);

        final String[] pieces = message.split("\\s");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mContainer.removeAllViews();
            }
        });

        for (String config : pieces) {
            final ImageView pieceImage = ChessPiece.create(MainActivity.this, config);

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mContainer.addView(pieceImage);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        dispatch("Ka1 Qb2 Bc3 Nd4 Re5 kf6 qg7 bh8 ng8 rf7");
    }
}
