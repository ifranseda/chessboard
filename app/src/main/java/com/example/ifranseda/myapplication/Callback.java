package com.example.ifranseda.myapplication;

import java.util.EventListener;

/**
 * Created by ifranseda on 3/27/16.
 */
public interface Callback extends EventListener {
    void onReceive(String message);
}
