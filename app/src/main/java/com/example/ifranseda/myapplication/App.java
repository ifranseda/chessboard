package com.example.ifranseda.myapplication;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by ifranseda on 3/27/16.
 */
public class App extends Application {

    private static Context mContext;
    private List<Callback> mCallbacks = new ArrayList<>();

    private Socket mSocket;
    private BufferedReader mBufferedReader;
    private boolean mRun = false;

    public static App getInstance() {
        return (App) mContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public void prepareSocket() {
        mRun = true;

        Executors.newFixedThreadPool(1).execute(new Runnable() {
            @Override
            public void run() {

                try {
                    InetAddress serverAddress = InetAddress.getByName("xinuc.org");
                    mSocket = new Socket();
                    mSocket.setKeepAlive(true);
                    mSocket.connect(new InetSocketAddress(serverAddress, 7387));

                    mBufferedReader = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));

                    while (mRun) {
                        String inputLine = mBufferedReader.readLine();
                        if (!TextUtils.isEmpty(inputLine)) {
                            dispatchMessage(inputLine);
                        }
                    }

                } catch (Exception e) {
                    Log.d("Error", "Unable to connect server");
                } finally {
                    try {
                        mBufferedReader.close();
                        mSocket.close();
                        Log.d("Close", "Socket closed");

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void stopSocket() {
        mRun = false;

        try {
            if (mBufferedReader != null) {
                mBufferedReader.close();
            }

            if (mSocket != null) {
                mSocket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        return mRun;
    }

    private void dispatchMessage(final String message) {
        Executors.newFixedThreadPool(10).execute(new Runnable() {
            @Override
            public void run() {
                for (Callback callback : mCallbacks) {
                    callback.onReceive(message);
                }
            }
        });
    }

    public void addListener(Callback callback) {
        mCallbacks.add(callback);
    }
}
